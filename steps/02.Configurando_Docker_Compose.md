## Qué es un  archivo Docker-compose?

Docker Compose es una herramienta que nos ayuda a ejecutar nuestra imagen de docker de un modo más sencillo desde la ubicación de nuestro proyecto. Por lo tanto podemos también administrar de modo más fácil los distintos servicios que componen nuestro proyecto. 

## Creando Docker Compose 
 Se debe crear en un archivo en la raíz que debe llamarse `docker-compose.yml`. Será un archivo yml que contendrá la configuración para todos los servicios que componen nuestro proyecto. 

 ### Paso 1
 La primera línea va a ser la versión de docker compose para la que vamos a  escribir nuestro archivo. En el momento del tutorial, la última versión era la 3, así que vamos a usar la misma. `version: "3"`

### Paso 2
Definimos los servicios que constituyen nuestra aplicación.
En este momento, sólo necesitamos un servicio para nuestra aplicación con *Python Django*
 
 ```
 Services:
    app: 
        build:
            context: .
        ports: 
            - "8000:8000"
        volumes:
            -./app:/app
        command: >
            sh -c "python manage.py runserver 0.0.0.0:8000"
 ```

Lo que estamos diciendo es: "Tenemos un servicio llamado *app* y la sección *build* de la configuración, vamos a setear su contexto a '.', que es nuestro directorio actual (desde el que ejecutamos el docker compose).
Los puertos (ports) van a ir desde el 8000 en nuestro host al 8000 en  nuestra imagen. 
Y debajo añadimos un volumen que nos permite obtener las actualizaciones que hacemos a nuestro proyecto en nuestra imagen de docker en tiempo real. 
Por lo que mapeamos el volumen de nuestra máquina local al contenedor de docker en el que ejecutaremos la aplicación. (Redundando, si cambiamos un archivo o lo que sea  en el proyecto, se cambiará automáticamente en el contenedor y no necesitamos reiniciar docker). Como siempre, el primer app en volumes hace referencia al directorio y el segundo a nuestra imagen de docker. 
Y finalmente tenemos el comando que se usará para ejecutar nuestra aplicación en nuestro contenedor de docker. Le ponen el símbolo ' >' para poder pasar el comando a la línea de abajo, por si hay más de uno, que no quede feo. Hay que vigilar la indentación. 
Explicación del comando `sh -c "python manage.py runserver 0.0.0.0:8000`: 
*sh* siginifica que se va a usar shell *-c* que va a ser un command y entre comillas va el comando. 
El comando ejecutará el server de desarrollo de Django en todas las direcciones ip que se ejecuten en el docker (esta parte la hace el 0.0.0.0) y se irá a ejecutar en el puerto 8000 el cual será mapeado a través de la configuración de los puertos en nuestra máquina local. 
De este modo podemos ejecutar nuestra aplicación y conectarla en el puerto 8000 de nuestra máquina local. 


## Ejecutando el Docker compose

Una vez guardado el archivo vamos a nuestra terminal y typeamos `docker-compose build`. Esto creará nuestra imagen usando la configuración de Docker compose. 
