
Lo que queremos hacer es usar Postgres en vez de la base de datos SQLITE que viene por defecto. 

## Cambios en el Docker-compose

Estos cambios nos ayudan a crear un servicio de base de datos y también a pasar unos marcos tanto para nuestra app como el database. 
Abrimos, por supuesto el archivo **docker-compose.yml** y empezamos por añadir la base de datos. 

Debajo del todo creamos un nuevo servicio llamado 'db' y le damos la imagen: `image: postgres:10-alpine` esto localiza la imagen en dockerhub y se la trae con la versión 10 alpine. 
A continuación setearemos variables de entorno. En la docu de dockerhub se pueden ver todas las opciones disponibles que pueden ser pasadas como variables de entorno. Aquí vamos a pasar variables para el nombre de usuario, el password y el nombre de la base de datos. 
Es importante que todo sea en mayúsculas puesto que es lo que espera Postgres cuando empieza. 
Con respecto al password, no usemos el mismo password que usaríamos en producción. Ya que esto se comitea. Por eso usamos uno chorra. 

Queda así: 
```
db:
    image: postgres:10-alpine
    environment: 
        - POSTGRES_DB=app
        - POSTGRES_USER=postgres
        - POSTGRES_PASSWORD=supersecretpassword
```


## Modificando el servicio app

Queremos establecer algunas variables de entorno y de paso depender de nuestra base de datos (significado).
Empezamos añadiendo variables a nuestra app. Por lo que debajo de command, añadimos environment. 
La primera que añadiremos será DB_HOST y necesita ser igual al nombre del servicio que va a runear nuestra base de datos (db, el servicio que hemos hecho en el punto anterior). 
El segundo será DB_NAME y va a ser postgres_db, por lo que lo igualaremos a app (tal y como lo tenemos en db).
DB_USER va a ser postgress. 
Y finalmente DB_PASS el password, nuevamente 'supersecretpassword', coincide con el de 'db'. 

```
environment : 
    - DB_HOST=db
    - DB_NAME=app
    - DB_USER=postgress
    - DB_PASS=supersecretpassword
```

## Depends on 

Cuando se ejecuta Docker-compose se pueden setear diferentes servicios para que dependan en otros servicios. Queremos nuestro servicio *app* que dependa del servicio  database.
Esto significa dos cosas: 

**1)** El servicio de database empezará antes que el servicio app
**2)** El servicio de databse estará disponible via network cuando usemos el hostname *db*. POr lo tanto cuando estemos dentro de nuestro servicio app, nos podremos conectar al hostname db y esto nos conectará a cualquier contenedor que este ejecutándose en nuestro servicio database. 

Debajo del setting de environment, vamos a añadir un *depends_on*  y dentro pondremos db. En ocasiones habrá más cosas (redis etc. ). 

```
 depends_on :
    - db
```


